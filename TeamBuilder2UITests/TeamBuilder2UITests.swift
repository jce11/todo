//
//  TeamBuilder2UITests.swift
//  TeamBuilder2UITests
//
//  Created by Jason Evans on 09/12/2016.
//  Copyright © 2016 Jason Evans. All rights reserved.
//

import XCTest

class TeamBuilder2UITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    func testdownloadLeagueData(){
        try! League.sharedInstance.getLeague()
        RunLoop.current.run(until: NSDate(timeIntervalSinceNow: 3) as Date)
        let  count = League.sharedInstance.countLeagues
        print(count)
        XCTAssertNotNil(count)
    }
    
    func testSampleLeagueData(){
        do {
            try! League.sharedInstance.getLeague()
            RunLoop.current.run(until: NSDate(timeIntervalSinceNow: 3) as Date)
            let  league = try! League.sharedInstance.returnLeague(atIndex: 1).caption
            print(league)
            XCTAssertEqual(league, "Premier League 2016/17")
        }
    }
    
    func testDownloadTeamData(){
        try! League.sharedInstance.getLeague()
        RunLoop.current.run(until: NSDate(timeIntervalSinceNow: 3) as Date)
        try! Team.sharedInstance.getTeam(atIndex: 0)
        RunLoop.current.run(until: NSDate(timeIntervalSinceNow: 3) as Date)
        let  count = Team.sharedInstance.countTeams
        print(count)
        XCTAssertNotNil(count)
    }
    
    func testSampleTeamData(){
        try! League.sharedInstance.getLeague()
        RunLoop.current.run(until: NSDate(timeIntervalSinceNow: 3) as Date)
        try! Team.sharedInstance.getTeam(atIndex: 0)
        RunLoop.current.run(until: NSDate(timeIntervalSinceNow: 3) as Date)
        
        do{
            let  TeamName = try! Team.sharedInstance.returnTeam(atIndex: 0).teamName
            print(TeamName)
            XCTAssertEqual(TeamName, "France")
        }catch TeamErrors.indexOutOfRange(let index){
            print(index)
        }
    }
    
    func testTeamURL(){
        let code = 1
        try! League.sharedInstance.getLeague()
        RunLoop.current.run(until: NSDate(timeIntervalSinceNow: 3) as Date)
        do{
            try! Team.sharedInstance.getTeam(atIndex: code)
            RunLoop.current.run(until: NSDate(timeIntervalSinceNow: 3) as Date)
            
        }catch TeamErrors.InvalidURL(let message){
            print("InvalidURL\(message)")
        }
    }
    
    
    func testDownloadPlayerData(){
        let code = 1
        try! League.sharedInstance.getLeague()
        RunLoop.current.run(until: NSDate(timeIntervalSinceNow: 3) as Date)
        try! Team.sharedInstance.getTeam(atIndex: code)
        RunLoop.current.run(until: NSDate(timeIntervalSinceNow: 3) as Date)
        try! Players.sharedInstance.getPlayer(select: code)
        
        do{
            let  player = Players.sharedInstance.countPlayers
            XCTAssertNotNil(player)
        }catch TeamErrors.indexOutOfRange(let index){
            print(index)
        }
        
    }
    
    func testGetPlayers(){
        //let expectations = expectation(description: "Wait for exception")
        let code = 1
        try! League.sharedInstance.getLeague()
        RunLoop.current.run(until: NSDate(timeIntervalSinceNow: 3) as Date)
        let team = try! League.sharedInstance.returnLeague(atIndex: 1)
        
        print("this is\(team)")
        try! Team.sharedInstance.getTeam(atIndex: code)
        RunLoop.current.run(until: NSDate(timeIntervalSinceNow: 3) as Date)
        let team1 = try! Team.sharedInstance.returnTeam(atIndex: 1)
        print("this is\(team1)")
        
        XCTAssertEqual(team.caption, "Premier League 2016/17")
        
        //try! self.players.getPlayer{ in
        //    expectations.fulfill()
        //    XCTAssertNotNil(self.players.countPlayers)
        //}
        // waitForExpectations(timeout: 5) { error in
        // }
    }
    
    
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
}
