//
//  TeamBuilder2Tests.swift
//  TeamBuilder2Tests
//
//  Created by Jason Evans on 09/12/2016.
//  Copyright © 2016 Jason Evans. All rights reserved.
//

import XCTest
@testable import TeamBuilder2

class TeamBuilder2Tests: XCTestCase {
    
    
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAddTeam(){
        
        
        let myTeam = MyTeams.sharedInstance
        
        
        
        let name = "ben"
        let position = "left"
        let jerseyNumber  = "7"
        let dateOfBirth  = "25/08/94"
        let nationality  = "english"
        let marketValue  =  "7,000,00"
        let club = "Barcelona"

        do{
           // try! myTeam.add(myteam: MyTeam(name: name))
           // try! myTeam.add(myteam: MyTeam()
            try! myTeam.add(myteam: MyTeam(name: name, position: position, jerseyNumber: jerseyNumber, dateOfBirth: dateOfBirth, nationality: nationality, marketValue: marketValue, club: club, crest: "empty", playerlink: ""))
            let result = try! myTeam.returnMyTeam(atIndex: 0)
            print(result)
            
            
          //  try! myTeam.add(myTeam: MyTeams(
        }
        

        
        
        
        
        
    }
    
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
