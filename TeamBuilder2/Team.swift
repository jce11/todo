//
//  Team.swift
//  TeamBuilder1
//
//  Created by Jason Evans on 07/12/2016.
//  Copyright © 2016 Jason Evans. All rights reserved.
//

import Foundation

struct Teams{
    //http://api.football-data.org/v1/competitions/426/teams
    var teamName: String
    var crest: String
    var playerLink: String
    
}

enum TeamErrors: Error {
    case InvalidURL(String)
    case InvalidKey(String)
    case InvalidArray
    case InvalidData
    case InvalidImage
    case indexOutOfRange(index: Int)
}

class Team{
    public static var sharedInstance = Team()
    
    private var searchTeam:[Teams]
    
    
    //initialise the stuct of teams
    private init(){
        searchTeam = []
    }
    
    
    /**
 returns the count of teams
 */
    public var countTeams: Int {
        get {
            return self.searchTeam.count
        }
    }
    
    
    /**
 will take a parameter (int) and return the team item in that index
     
     @atIndex is an Int and will return this value from the team instance
 */
    public func returnTeam(atIndex: Int) throws -> Teams{
        if(atIndex < 0 || atIndex > (self.searchTeam.count - 1)){
            throw TeamErrors.indexOutOfRange(index: atIndex)
        }
        return self.searchTeam[atIndex]
    }
    
    /**
 a function to call an api and download team data based on the selection of the users. The user will imput the league they have chosen and this will retrieve all teams in that league
     
     @atIndex is parsed through from the selected row of the table view
     @throws will allow me to throw an error based on the struct i declared
 */
    public func getTeam(atIndex: Int) throws -> Void{
        //print(League.sharedInstance.countLeagues)
        
        if(atIndex < 0 || atIndex > (League.sharedInstance.countLeagues - 1)){
            throw TeamErrors.indexOutOfRange(index: atIndex)
        }
        
        let id = try! League.sharedInstance.returnLeague(atIndex: atIndex).id
        
        //print("http://api.football-data.org/v1/competitions/\(code)/teams")
        let jsonUrl: String = "http://api.football-data.org/v1/competitions/\(id)/teams"
        print("http://api.football-data.org/v1/competitions/\(id)/teams")
        // NSURL sessions allow us to download data using HTTP for APIs
        // a NSURL which contains a correct resourse
        guard let url = URL(string: jsonUrl) else {
            print("error creating string")
            throw TeamErrors.InvalidURL(jsonUrl)
        }
        var leagueURL = URLRequest(url: url)
        leagueURL.addValue("032cd4585d89432688f29fee1dffab22", forHTTPHeaderField: "X-Auth-Token")
        leagueURL.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: leagueURL) {data, responce, error in
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                self.searchTeam = []
                print(data!)
                for item in json["teams"]! as! [[String: AnyObject]]  {
                    
                    guard let name = ((item["name"]!) as? String)else{
                        print("error no name for this item")
                        throw TeamErrors.InvalidKey("Invalid Team Name")
                    }
                    
                    let crestUrl = ((item["crestUrl"]!) as? String) ?? ("https://www.google.co.uk/imgres?imgurl=http%3A%2F%2Fwww.jordans.com%2F~%2Fmedia%2Fjordans%2520redesign%2Fno-image-found.ashx%3Fh%3D275%26la%3Den%26w%3D275%26hash%3DF87BC23F17E37D57E2A0B1CC6E2E3EEE312AAD5B&imgrefurl=http%3A%2F%2Fwww.jordans.com%2Fabout-us%2Fstore-locations%2Freading&docid=hhC7rPjmHDnYYM&tbnid=-pCCN7e53SUTOM%3A&vet=1&w=275&h=275&safe=off&bih=653&biw=1201&ved=0ahUKEwjsuuubrd3QAhULBMAKHQUvACIQMwgeKAIwAg&iact=mrc&uact=8")
                    
                    guard let link = (item["_links"]!) as? NSDictionary else{
                        throw TeamErrors.InvalidKey("invalid player link")
                    }
                    let playerLink = (link["players"]) as? NSDictionary
                    let playerLinks = ((playerLink?["href"]) as? String) ?? ("No Team URL")
                    self.searchTeam.append(Teams(teamName: name, crest: crestUrl, playerLink: playerLinks))
                }
            }catch{
                print("error thrown: \(error)")
            }
            print("Num of Teams \(self.countTeams)")
            
        }
        task.resume()
        
    }
    
}
