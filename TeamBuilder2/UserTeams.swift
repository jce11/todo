//
//  MyTeam.swift
//  TeamBuilder2
//
//  Created by Jason Evans on 11/12/2016.
//  Copyright © 2016 Jason Evans. All rights reserved.
//

import Foundation

struct MyTeam{
    var name: String
    var position: String
    var jerseyNumber: String
    var dateOfBirth: String
    var nationality: String
    var marketValue: String
    var club: String
    var crest: String
    var playerlink: String
}
enum MyTeamErrors: Error {
    case indexOutOfRange(index: Int)
}

class MyTeams{
    public static var sharedInstance = MyTeams()
    
    var searchMyTeam:[MyTeam]
    
    private init() {
        searchMyTeam = []
    }
    
    public var countPlayers: Int {
        get {
            return self.searchMyTeam.count
        }
    }
    
    /**
 Func savelist allows me to save the team that the user has selected into NSuserdefaults
     i take the selected rows from the league, team and player and create a player link key 
     then add this to an array and save to userDefaults
 */
    func saveList(){
        var savedTeam:[String] = []
        for item in self.searchMyTeam{
            savedTeam.append(item.playerlink)
        }
        
        let savedTeams = UserDefaults.standard
        savedTeams.set(savedTeam.self, forKey: "teams")
        savedTeams.synchronize()
        print("List has been saved")
    }
    /**
 Func loadlist will load the team list the user creates
    
     @userTeams = a temp variable to extract data from userdefaults
     @club = a temp variable to store club name
     @crest = a temp cariable to store crest link
     
     I need to be able to acces each character in the sting individually so using the character loop i iterate through each one, calling the get functions of league, team and player finsishing by adding the selected player to MyTeam
 */
    func loadList(){
        let savedItems = UserDefaults.standard
        if let userTeams = savedItems.object(forKey: "teams") as! [String]?{
            print("data loaded")
            print(userTeams)
            var club = ""
            var crest = ""
            for item in userTeams{
                var count = 0
                for character in item.characters{
                    let char = String(character)
                    var num = Int(char)
                    print("This is num: \(num!)")
                    if count == 0{
                        try! League.sharedInstance.getLeague()
                        num = num! + 1
                        RunLoop.current.run(until: NSDate(timeIntervalSinceNow: 0.2) as Date)
                        try! Team.sharedInstance.getTeam(atIndex: num!)
                        RunLoop.current.run(until: NSDate(timeIntervalSinceNow: 0.2) as Date)
                        count += 1
                    }else if count == 1{
                        print("count = 1 \(count)")
                        
                        
                        try! Players.sharedInstance.getPlayer(select: num!)
                        crest = try! Team.sharedInstance.returnTeam(atIndex: num!).crest
                        club = try! Team.sharedInstance.returnTeam(atIndex: num!).teamName
                        RunLoop.current.run(until: NSDate(timeIntervalSinceNow: 0.2) as Date)
                        print(Players.sharedInstance.countPlayers)
                        count += 1
                    }else if count == 2{
                        let loaded = try! Players.sharedInstance.returnPlayer(atIndex: num!)
                        RunLoop.current.run(until: NSDate(timeIntervalSinceNow: 0.2) as Date)
                        
                        try! MyTeams.sharedInstance.add(myteam: MyTeam.init(name: loaded.name, position: loaded.position, jerseyNumber: loaded.jerseyNumber, dateOfBirth: loaded.dateOfBirth, nationality: loaded.nationality, marketValue: loaded.marketValue, club: club , crest: crest, playerlink: item))
                    }
                    
                }
            }
            
        }
    }
    
    /**
 func returnMyTeam will take 
     @atIndex: an int which will be the selected player from table view
     @throws checks to see if the passed data is a valid index
 */
    public func returnMyTeam(atIndex: Int) throws -> MyTeam{
        if(atIndex < 0 || atIndex > (self.searchMyTeam.count - 1)){
            throw MyTeamErrors.indexOutOfRange(index: atIndex)
        }
        return self.searchMyTeam[atIndex]
    }
    
    /**
 func add will all the user to add a player to their team, using the MyTeam constructer
     @myteam this is the struct of player details that will be passed back and added
 */
    public func add(myteam: MyTeam)throws{
        self.searchMyTeam.append(myteam)
    }
    
    /**
 Clear all items in the list using remove all function
 */
    public func clearTeams(){
        self.searchMyTeam.removeAll()
    }
    /**
 RemovePlayer 
     @atIndex is an integer from tableview 
     
     will use integer to determine which player to delete
 */
    public func removePlayer(atIndex: Int) throws{
        if(atIndex < 0 || atIndex > (self.searchMyTeam.count - 1)){
            throw MyTeamErrors.indexOutOfRange(index: atIndex)
        }
        self.searchMyTeam.remove(at: atIndex)
    }
    
    
    
    
    
}
