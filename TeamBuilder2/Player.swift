//
//  Player.swift
//  TeamBuilder1
//
//  Created by Jason Evans on 07/12/2016.
//  Copyright © 2016 Jason Evans. All rights reserved.
//

import Foundation

struct Player{
    var name: String
    var position: String
    var jerseyNumber: String
    var dateOfBirth: String
    var nationality: String
    var marketValue: String
}

enum PlayerErrors: Error {
    case InvalidURL(String)
    case InvalidKey(String)
    case InvalidArray
    case InvalidData
    case InvalidImage
    case indexOutOfRange(index: Int)
}

class Players{
    public static var sharedInstance = Players()
    
    private var searchPlayer:[Player]
    
    private init() {
        searchPlayer = []
    }
    
    public var countPlayers: Int {
        get {
            return self.searchPlayer.count
        }
    }
    
    /**
 return player will take an intger the user has selected and return a player in the database from that index
     @atIndex integer from tableview for selected player
     @throws using if statements checks that the atIndex variable is valid and in the database
     
     returns the player at that index
 */
    public func returnPlayer(atIndex: Int) throws -> Player{
        if(atIndex < 0 || atIndex > (self.searchPlayer.count - 1)){
            throw PlayerErrors.indexOutOfRange(index: atIndex)
        }
        return self.searchPlayer[atIndex]
    }
    
    
    /**
 getPlayer will use a data session and api call to download player data.
     @select is the index of the player that has been selected from tableview
     @code is the url of the target player and is taken from the team struct
     
     the api key is attched and the session is started, the data is then extracted using for loops, as the data was tricky to extract due to multiple dictionaries inside arrays ther are many for loops
     
     each piece of data is added to  variables using the gaurd statements to unwrap and then inserted into the player database
     
 */
    public func getPlayer(select: Int) throws -> Void{
        
        let code = try! Team.sharedInstance.returnTeam(atIndex: select).playerLink
        print("This is the code selected\(code)")
        let jsonUrl: String = code
        
        //print(jsonUrl)
        // NSURL sessions allow us to download data using HTTP for APIs
        // a NSURL which contains a correct resourse
        guard let url = URL(string: jsonUrl) else {
            print("error creating string")
            throw PlayerErrors.InvalidURL(jsonUrl)
        }
        var leagueURL = URLRequest(url: url)
        leagueURL.addValue("032cd4585d89432688f29fee1dffab22", forHTTPHeaderField: "X-Auth-Token")
        leagueURL.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: leagueURL) {data, response, error in
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                
                self.searchPlayer = []
                
                for item in json["players"] as! [[String: AnyObject]]{
                    guard let name = ((item["name"]!) as? String) else{
                        throw PlayerErrors.InvalidKey("invalid player name")
                    }
                    guard let position = ((item["position"]!) as? String) else{
                        throw PlayerErrors.InvalidKey("invalid player position")
                    }
                    let jerseyNumber = ( ((item["jerseyNumber"]!) as? String) ?? ("No Assigned jersey number") )
                    
                    guard let dateOfBirth = ((item["dateOfBirth"]!) as? String) else{
                        throw PlayerErrors.InvalidKey("invalid player DOB")
                    }
                    guard let nationality = ((item["nationality"]!) as? String) else{
                        throw PlayerErrors.InvalidKey("invalid player DOB")
                    }
                    let marketvalue = ( ((item["marketValue"]!) as? String) ?? ("Market value info not available"))
                    self.searchPlayer.append(Player(name: name, position: position, jerseyNumber: jerseyNumber, dateOfBirth: dateOfBirth, nationality: nationality, marketValue: marketvalue))
                }
            }catch{
                print("error thrown: \(error)")
            }
            print("Num of Players \(self.searchPlayer.count)")
            //  print("TEST: \(self.searchPlayer.Plyer["name"])")
        }
        task.resume()
        
    }
    
    
}
