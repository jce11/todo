//
//  PlayerController.swift
//  TeamBuilder2
//
//  Created by Jason Evans on 09/12/2016.
//  Copyright © 2016 Jason Evans. All rights reserved.
//

import UIKit

class PlayerController: UIViewController {
    
    public var playerID:Int = 0
    public var userviews:Int = 0
    public var teamSelect:Int = 0
    public var leagueSelect:Int = 0
    
    
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var content: UIView!
    @IBOutlet weak var main: UIView!
    @IBOutlet weak var details: UIView!
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var playerName: UILabel!
    @IBOutlet weak var teamName: UILabel!
    
    @IBOutlet weak var crest: UIImageView!
    @IBOutlet weak var position: UILabel!
    @IBOutlet weak var number: UILabel!
    @IBOutlet weak var nationality: UILabel!
    @IBOutlet weak var DOB: UILabel!
    @IBOutlet weak var marketValue: UILabel!
    @IBOutlet weak var addPlayerTitle: UIBarButtonItem!
    
    /**
    Add Player function
     @playerlink is created using the selection of league, team and player, put into a string
     
     The current player is then added to MyTeam using the MyTeam.add function using the text labels of th current player.
     
     The list is then saved to nsUserDefaults using saveList function
     
     Alert is shown on screen to show the user the data has been succefully added
    */
    @IBAction func addPlayer(_ sender: UIBarButtonItem) {
        let Playerlink = ("\(leagueSelect)\(teamSelect)\(playerID)")
        print(Playerlink)
        
        
        try! MyTeams.sharedInstance.add(myteam: MyTeam(name: self.playerName.text!, position: self.position.text!, jerseyNumber: self.position.text!, dateOfBirth: self.DOB.text!, nationality: self.nationality.text!, marketValue: self.marketValue.text!, club: self.teamName.text!, crest: (try! Team.sharedInstance.returnTeam(atIndex: teamSelect).crest), playerlink: Playerlink))
        
        
        MyTeams.sharedInstance.saveList()
        
        let alert = UIAlertController(title: "successfully added", message: "", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "okay", style: .default, handler: nil))
        
        
        
        present(alert, animated: true, completion: nil)
        print(try! MyTeams.sharedInstance.returnMyTeam(atIndex: 0))
    }
    
    
    /**
     Based on userview the player details screen will show either:
     the player selected from myTeam or the player selected from the player select menu.
     
     each label is changed depending on myTeam or the current players in menu
     
     The frame of the view is also auto resied at the end of view did load to make sure auto layout is functioning correctly
    */
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if userviews == 2{
            self.title = ""
            addPlayerTitle.title = "Add to team"
            self.playerName.text = try! Players.sharedInstance.returnPlayer(atIndex: playerID).name
            self.teamName.text = try! Team.sharedInstance.returnTeam(atIndex: teamSelect).teamName
            
            //self.title = try! Players.sharedInstance.returnPlayer(atIndex: playerID).name
            
            self.position.text = try! Players.sharedInstance.returnPlayer(atIndex: playerID).position
            self.number.text = try! Players.sharedInstance.returnPlayer(atIndex: playerID).jerseyNumber
            self.DOB.text = try! Players.sharedInstance.returnPlayer(atIndex: playerID).dateOfBirth
            self.nationality.text = try! Players.sharedInstance.returnPlayer(atIndex: playerID).nationality
            self.marketValue.text = try! Players.sharedInstance.returnPlayer(atIndex: playerID).marketValue
            self.loadImage(withID: teamSelect)
            
            
        }else{
            addPlayerTitle.title = ""
            self.title = ""
            self.playerName.text = try! MyTeams.sharedInstance.returnMyTeam(atIndex: playerID).name
            print(try! MyTeams.sharedInstance.returnMyTeam(atIndex: playerID).name)
            self.teamName.text = try! MyTeams.sharedInstance.returnMyTeam(atIndex: playerID).club
            self.position.text = try! MyTeams.sharedInstance.returnMyTeam(atIndex: playerID).position
            self.number.text = try! MyTeams.sharedInstance.returnMyTeam(atIndex: playerID).jerseyNumber
            self.DOB.text = try! MyTeams.sharedInstance.returnMyTeam(atIndex: playerID).dateOfBirth
            self.nationality.text = try! MyTeams.sharedInstance.returnMyTeam(atIndex: playerID).dateOfBirth
            self.loadImages(withID: (try! MyTeams.sharedInstance.returnMyTeam(atIndex: playerID).crest))
        }
        DispatchQueue.main.async {
            let frameHeight = self.content.frame.height + 25
            print("frame height \(frameHeight)")
            let size = CGSize(width: self.main.frame.width, height: frameHeight)
            self.content.frame.size = size
            self.scroll.contentSize = size
            
            
            
            
        }
        
        
        
        
        // Do any additional setup after loading the view.
    }
    
    /**
 load image will load the crest of the team
     @playerID is the player selected and is used to get the link of the crest from the team data
     a data session is started and the data is downloaded from the api.
     the data(image) is then set to = the UIImage
 */
    func loadImage(withID playerID: Int){
        let urlString = try! Team.sharedInstance.returnTeam(atIndex: playerID).crest
        print(urlString)
        let url = URL(string: urlString)
        let data = try? Data(contentsOf: url!)
        RunLoop.current.run(until: NSDate(timeIntervalSinceNow: 1) as Date)
        print(data!)
        //self.teamCrest.image = UIImage(data: data!)
        //self.teamCrest.image = UIImage(data: data!)
        DispatchQueue.main.async {
            // self.image.image = UIImage(data: data!)
            self.crest.image = UIImage(data: data!)
        }
    }
    
    /**
     load image will load the crest of the team
     @playerID is a url link from the myteam struct 
     a data session is started and the data is downloaded from the api.
     the data(image) is then set to = the UIImage
     */
    func loadImages(withID playerID: String){
        let urlString = playerID
        print(urlString)
        let url = URL(string: urlString)
        let data = try? Data(contentsOf: url!)
        RunLoop.current.run(until: NSDate(timeIntervalSinceNow: 1) as Date)
        print(data!)
        //self.teamCrest.image = UIImage(data: data!)
        //self.teamCrest.image = UIImage(data: data!)
        DispatchQueue.main.async {
            // self.image.image = UIImage(data: data!)
            self.crest.image = UIImage(data: data!)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
