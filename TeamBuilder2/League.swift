//
//  File.swift
//  TeamBuilder
//
//  Created by Jason Evans on 30/11/2016.
//  Copyright © 2016 Jason Evans. All rights reserved.
//

import Foundation

struct Leagues{
    //http://api.football-data.org/v1/competitions
    var id: Int
    var caption: String
    var numTeams: Double
    
}

enum LeagueErrors: Error {
    case InvalidURL(String)
    case InvalidKey(String)
    case InvalidArray
    case InvalidData
    case InvalidImage
    case indexOutOfRange(index: Int)
}


class League{
    public static var sharedInstance = League()
    
    private var searchLeague:[Leagues]
    
    private init() {
        searchLeague = []
    }
    
    
    /**
 Count leagues 
     will could all the items in the league stuct
 */
    public var countLeagues: Int {
        get {
            return self.searchLeague.count
        }
    }
    
    /**
 return League function
     @atindex is an integer based on user selection in tableview
     @throws will throw an error based on atindex if the integer is not valid
     
     returns the league that has been selected
 */
    public func returnLeague(atIndex: Int) throws -> Leagues{
        if(atIndex < 0 || atIndex > (self.searchLeague.count - 1)){
            throw LeagueErrors.indexOutOfRange(index: atIndex)
        }
        return self.searchLeague[atIndex]
    }
    
    
    /**
 GetLeague will use an api call to download league data
     @jsonURL is set to the url of the api
     
     guard statement used to check if the url is valid
     the session is started and the api call is made
     
     @json data from the session is stored in the variable json
     
     json is then extracted using a for loop go get the caption id and number of teams
 */
    public func getLeague() throws -> Void{
        let jsonUrl: String = "http://api.football-data.org/v1/competitions"
        //print(jsonUrl)
        // NSURL sessions allow us to download data using HTTP for APIs
        // a NSURL which contains a correct resourse
        guard let url = URL(string: jsonUrl) else {
            print("error creating string")
            throw LeagueErrors.InvalidURL(jsonUrl)
        }
        var leagueURL = URLRequest(url: url)
        leagueURL.addValue("032cd4585d89432688f29fee1dffab22", forHTTPHeaderField: "X-Auth-Token")
        leagueURL.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: leagueURL) {data, response, error in
            do {
                guard let jsondata = (data) else{
                    print("error in data")
                    throw LeagueErrors.InvalidKey("DATA ERROR")
                }
                let json = try JSONSerialization.jsonObject(with: jsondata, options: JSONSerialization.ReadingOptions.mutableContainers)
                self.searchLeague = []
                for item in json as! [AnyObject] {
                    //print(item["caption"]!)
                    //print(item["id"]!)
                    //print(item["numberOfTeams"]!)
                    let caption = (item["caption"]!) as? String
                    let id = (item["id"]!) as? Int
                    let numteams = (item["numberOfTeams"]!) as? Double
                    self.searchLeague.append(Leagues(id: id!, caption: caption!, numTeams: numteams!))
                }
            }catch{
                print("error thrown: \(error)")
            }
            print("Num of leagues \(self.searchLeague.count)")
            
            
        }
        task.resume()
        
        
    }
}


