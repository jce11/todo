//
//  LeagueController.swift
//  TeamBuilder2
//
//  Created by Jason Evans on 09/12/2016.
//  Copyright © 2016 Jason Evans. All rights reserved.
//

import UIKit

class LeagueController: UITableViewController {
    
    public var userviews:Int = 0
    public var teamSelect:Int = 0
    public var leagueSelect:Int = 0
    
    @IBOutlet weak var backTitle: UIBarButtonItem!
    
    @IBAction func showMyTeam(_ sender: UIBarButtonItem) {
        self.title = "My Team"
        backTitle.title = "Search player"
        userviews = 3
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    
    /**
     If the user is viewing teams in player select and button is clicked:
     back buttin should be hidden
     userview - 1
     
     If the user is viewing players and button is clicked
     back button should say leagues
     userview - 1
     
     If the user is viewing my team and button is clicked
     back button should show nothing
     the title should be set to pick player
     
     
     
 */
    @IBAction func back(_ sender: UIBarButtonItem) {
        
        if userviews == 1{
            userviews = (userviews - 1)
            backTitle.title = ""
          //  MyTeams.sharedInstance.loadList()
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }else if userviews == 2 {
            userviews = (userviews - 1)
            backTitle.title = "Leages"
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }else if userviews == 3{
            userviews = 0
            backTitle.title = ""
            self.title = "Pick Player"
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
/**
     set the network activity on when downloading from the api
     the api call is made so that the leagues can be shown straight away in the app
     set the title to player selet and back title to hidden
     load the list of player from user data for data persistence
 */
    override func viewDidLoad() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        try! League.sharedInstance.getLeague()
        RunLoop.current.run(until: NSDate(timeIntervalSinceNow: 1) as Date)
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        self.title = "Player select"
        self.backTitle.title = ""
        MyTeams.sharedInstance.loadList()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    
    /**
 Return the number of rows based on where the user is in the app for each my teams, teams, players and leagues
 */
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        var returncount = 0
        if userviews == 0{
            returncount = League.sharedInstance.countLeagues
        }else if userviews == 1{
            returncount = Team.sharedInstance.countTeams
        }else if userviews == 2{
            returncount = Players.sharedInstance.countPlayers
        }else if userviews == 3{
            returncount = MyTeams.sharedInstance.countPlayers
        }
        
        return returncount
    }

    /**
     change the labels in the table view depending on where the user is in the app
     if user is in legues (userview = 0)
     show league labels
     if user is in teams (userview = 1)
     show team labels
     if user is in players (userview = 2)
     show player labels
     if user is in myteam (userview = 3)
     show myteam labels
 */
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeagueName", for: indexPath)
        if userviews == 0{
            if let label = cell.textLabel {
                //try! Players.sharedInstance.getLeague()
                label.text = try? League.sharedInstance.returnLeague(atIndex: indexPath.row).caption
            }
        }else if userviews == 1{
            if let label = cell.textLabel {
                //try! Players.sharedInstance.getLeague()
                label.text = try? Team.sharedInstance.returnTeam(atIndex: indexPath.row).teamName
            }
        }else if userviews == 2{
            if let label = cell.textLabel {
                //try! Players.sharedInstance.getLeague()
                label.text = try? Players.sharedInstance.returnPlayer(atIndex: indexPath.row).name
            }
        }else if userviews == 3{
            if let label = cell.textLabel {
                label.text = try? MyTeams.sharedInstance.returnMyTeam(atIndex: indexPath.row).name
            }
        }
        // Configure the cell...

        return cell
    }
    
    /**
     this is the selected part of the app. 
     if userview = 0
      download team data for selected league (show and unshow network indicator)
     @leagueSelect variable is created to create @playerlink as reference
     userviews is set to 1 
     
     if userview = 1
     download player data for selected team(show and uncshow network indicator)
     @teamselect is set to the selected team to create @playerlink as reference for nsuserdata later
     userviews is set to 2
     
     if userview = 2
     show the player detail view 
     
     if userview = 3
     show the player detail view
     
     reload the data at the end of the if statements to update the table view controller
 */
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("row selected\(indexPath.row)")
        if userviews == 0{
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            try! Team.sharedInstance.getTeam(atIndex: indexPath.row)
            RunLoop.current.run(until: NSDate(timeIntervalSinceNow: 1) as Date)
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            self.backTitle.title = "Leagues"
            leagueSelect = indexPath.row
            userviews = 1
        }else if userviews == 1{
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            try! Players.sharedInstance.getPlayer(select: indexPath.row)
            RunLoop.current.run(until: NSDate(timeIntervalSinceNow: 1) as Date)
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            self.backTitle.title = "Teams"
            userviews = 2
            teamSelect = indexPath.row
            print("team select is:\(teamSelect)")
        }else if userviews == 2{
            performSegue(withIdentifier: "showPlayer", sender: nil)
        }else if userviews == 3{
            self.backTitle.title = "Search Player"
            performSegue(withIdentifier: "showPlayer", sender: nil)
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    /**
 prepare the tableview controller for the detail view and allows me to check if the destination is correct and segue is set up corectly
     @PlayerID is the selected player
     @userviews is the current view the user has
     @teamselect is the selected team
 */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPlayer" {
            print("segue with \(segue.identifier) identifier triggered")
            if let row = tableView.indexPathForSelectedRow?.row{
                print("found row\(row)")
                if let navigationController = segue.destination as? UINavigationController {
                    if let PlayerController = navigationController.topViewController as? PlayerController {
                        print("found controller")
                        PlayerController.playerID = row
                        PlayerController.userviews = userviews
                        PlayerController.teamSelect = teamSelect
                        
                    }
                }
            }
        }
        
        
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    

}
